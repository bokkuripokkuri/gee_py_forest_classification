# %%
"""
<table class="ee-notebook-buttons" align="left">
    <td><a target="_blank"  href="https://github.com/giswqs/geemap/tree/master/examples/template/template.ipynb"><img width=32px src="https://www.tensorflow.org/images/GitHub-Mark-32px.png" /> View source on GitHub</a></td>
    <td><a target="_blank"  href="https://nbviewer.jupyter.org/github/giswqs/geemap/blob/master/examples/template/template.ipynb"><img width=26px src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/883px-Jupyter_logo.svg.png" />Notebook Viewer</a></td>
    <td><a target="_blank"  href="https://colab.research.google.com/github/giswqs/geemap/blob/master/examples/template/template.ipynb"><img src="https://www.tensorflow.org/images/colab_logo_32px.png" /> Run in Google Colab</a></td>
</table>
"""

# %%
"""
## Install Earth Engine API and geemap
Install the [Earth Engine Python API](https://developers.google.com/earth-engine/python_install) and [geemap](https://geemap.org). The **geemap** Python package is built upon the [ipyleaflet](https://github.com/jupyter-widgets/ipyleaflet) and [folium](https://github.com/python-visualization/folium) packages and implements several methods for interacting with Earth Engine data layers, such as `Map.addLayer()`, `Map.setCenter()`, and `Map.centerObject()`.
The following script checks if the geemap package has been installed. If not, it will install geemap, which automatically installs its [dependencies](https://github.com/giswqs/geemap#dependencies), including earthengine-api, folium, and ipyleaflet.
"""

# %%
# Installs geemap package
import subprocess

try:
    import geemap
except ImportError:
    print("Installing geemap ...")
    subprocess.check_call(["python", "-m", "pip", "install", "geemap"])

# %%
import ee
import geemap

# %%
"""
## Create an interactive map 
The default basemap is `Google Maps`. [Additional basemaps](https://github.com/giswqs/geemap/blob/master/geemap/basemaps.py) can be added using the `Map.add_basemap()` function. 
"""

# %%
Map = geemap.Map(center=[40, -100], zoom=4)
Map

# %%
"""
## Add Earth Engine Python script 
"""

# %%
# Add Earth Engine dataset
csle = 1

def prin():
  console.log("----------")
  console.log(csle)
  return csle++



#　（１）背景画像表示
#　2018年1月のランドサット画像を選択
image = ee.Image(ee.ImageCollection('LANDSAT/LC08/C01/T1_TOA') \
  .filterDate('2020-08-01', '2020-12-31') \
  .filterBounds(ee.Geometry.Point(131.756020, 34.301325)) \
  .sort('CLOUD_COVER') \
  .first())


#　ランドサット可視画像のパンシャープン（高精細化）
rgb = image.select('B4', 'B3', 'B2')
pan = image.select('B8')
huesat = rgb.rgbToHsv().select('hue', 'saturation')
upres = ee.Image.cat(huesat, pan).hsvToRgb()


#　画像の表示
# Map.addLayer(rgb, {max: 0.3}, 'rgb_Original')
Map.addLayer(upres, {'max': 0.3}, 'Pansharpened')


#　（２）地表面温度の時系列変化

#　値を調べる場所を指定
Sugi = ee.Feature(
    ee.Geometry.Rectangle(131.756020, 34.301325, 131.756146, 34.301156),
    {'label': 'Sugi1'})
Hinoki = ee.Feature(
    ee.Geometry.Rectangle(131.761772, 34.299037, 131.761878, 34.298997),
    {'label': 'Hinoki1'})
Bamboo = ee.Feature(
    ee.Geometry.Rectangle(131.788620, 34.274838, 131.788923, 34.274768),
    {'label': 'Bamboo1'})
Regions = ee.FeatureCollection([Sugi, Hinoki, Bamboo])


#温度を調べる場所をマップで表示
COLOR = {
  'c1': 'ff0000',
  'c2': '0000ff',
  'c3': '00ff00'}


# Map.addLayer(Hinoki, {color: COLOR.c2},"Hinoki1")
# Map.addLayer(Bamboo, {color: COLOR.c3},"Bamboo1")
Map.setCenter(131.756020, 34.301325, 11)



Map.addLayer(Sugi, {'color': COLOR.c1},"Sugi1")


S2images = ee.ImageCollection('COPERNICUS/S2') \
    .filterDate("2018-01-01","2018-03-31")

# # NDVI用の単バンドを取り出す

def func_sbp(image):
    return image.normalizedDifference(['B8','B4']).rename('NDVI')

NDVI = S2images.map(func_sbp)





#NDVIをビジュアライズするための設定

Map.addLayer(eeObject = NDVI,
              visParams={"opacity":1,
              "min":-1,
              "max":1,
              "palette":["ff0000","efff00","27ff00","00ffdb","0013ff"]},
              name="NDVI")


NDVI_TimeSeriesChart = ui.Chart.image.series({
    'imageCollection': NDVI,
    'region': Sugi,
    'reducer': ee.Reducer.mean(),#指定範囲Rectangleの平均値を取得
    'xProperty': 'system:time_start',
    'seriesProperty': 'label',
    'scale': 200})


    NDVI_TimeSeriesChart.setChartType('ScatterChart')
    NDVI_TimeSeriesChart.setOptions({
     'title': 'Temporal sequence of surface temperature in the Himalayas',
     'vAxis': {
       'title': 'Temperature (Celsius)'
     },
     'lineWidth': 1,
     'pointSize': 4,
     'series': {
       '0': '{color': COLOR.c1},
     }
    })
    print(NDVI_TimeSeriesChart)

# chart.style().set({position: 'bottom-right',
#                   width: '500px',
#                   height: '300px'})
# Map.add(chart)

# 時

# # Landsat8であれば、雲のMaskを実行する関数
# def mask(image):
#   qa = image.select('BQA')

#   mask = qa.bitwiseAnd(1 << 4).eq(0)
#   return image.updateMask(mask)
#


# img = landsat8Toa.first()








# %%
"""
## Display Earth Engine data layers 
"""

# %%
Map.addLayerControl()  # This line is not needed for ipyleaflet-based Map.
Map

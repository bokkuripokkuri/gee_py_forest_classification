var csle = 1

function prin(){
  console.log("----------");
  console.log(csle);
  return csle++;
}


//　（１）背景画像表示
//　2018年1月のランドサット画像を選択
var image = ee.Image(ee.ImageCollection('LANDSAT/LC08/C01/T1_TOA')
  .filterDate('2020-08-01', '2020-12-31')
  .filterBounds(ee.Geometry.Point(131.756020, 34.301325))
  .sort('CLOUD_COVER')
  .first());


//　ランドサット可視画像のパンシャープン（高精細化）
var rgb = image.select('B4', 'B3', 'B2');
var pan = image.select('B8');
var huesat = rgb.rgbToHsv().select('hue', 'saturation');
var upres = ee.Image.cat(huesat, pan).hsvToRgb();


//　画像の表示
// Map.addLayer(rgb, {max: 0.3}, 'rgb_Original');
Map.addLayer(upres, {max: 0.3}, 'Pansharpened');


//　（２）地表面温度の時系列変化

//　値を調べる場所を指定
var Sugi = ee.Feature(    
    ee.Geometry.Rectangle(131.756020, 34.301325, 131.756146, 34.301156),
    {label: 'Sugi1'});
var Hinoki = ee.Feature(  
    ee.Geometry.Rectangle(131.761772, 34.299037, 131.761878, 34.298997),
    {label: 'Hinoki1'});
var Bamboo = ee.Feature(  
    ee.Geometry.Rectangle(131.788620, 34.274838, 131.788923, 34.274768),
    {label: 'Bamboo1'});
var Regions = new ee.FeatureCollection([Sugi, Hinoki, Bamboo]);


//温度を調べる場所をマップで表示
var COLOR = {
  c1: 'ff0000',
  c2: '0000ff',
  c3: '00ff00'};
  

// Map.addLayer(Hinoki, {color: COLOR.c2},"Hinoki1");
// Map.addLayer(Bamboo, {color: COLOR.c3},"Bamboo1");
Map.setCenter(131.756020, 34.301325, 11);



Map.addLayer(Sugi, {color: COLOR.c1},"Sugi1");


var S2images = ee.ImageCollection('COPERNICUS/S2')
    .filterDate("2018-01-01","2018-03-31");

// // NDVI用の単バンドを取り出す
var NDVI = S2images.map(function(image){
    return image.normalizedDifference(['B8','B4']).rename('NDVI')
    });


//NDVIをビジュアライズするための設定

Map.addLayer(eeObject = NDVI, 
              visParams={"opacity":1,
              "min":-1,
              "max":1,
              "palette":["ff0000","efff00","27ff00","00ffdb","0013ff"]},
              name="NDVI");


var NDVI_TimeSeriesChart = ui.Chart.image.series({
    imageCollection: NDVI,
    region: Sugi,
    reducer: ee.Reducer.mean(),//指定範囲Rectangleの平均値を取得
    xProperty: 'system:time_start',
    seriesProperty: 'label',
    scale: 200});


    NDVI_TimeSeriesChart.setChartType('ScatterChart');
    NDVI_TimeSeriesChart.setOptions({
     title: 'Temporal sequence of surface temperature in the Himalayas',
     vAxis: {
       title: 'Temperature (Celsius)'
     },
     lineWidth: 1,
     pointSize: 4,
     series: {
       0: {color: COLOR.c1},
     }
    });
    print(NDVI_TimeSeriesChart);

// chart.style().set({position: 'bottom-right',
//                   width: '500px',
//                   height: '300px'});
// Map.add(chart);

// 時

// // Landsat8であれば、雲のMaskを実行する関数
// var mask = function(image){
//   var qa = image.select('BQA');

//   var mask = qa.bitwiseAnd(1 << 4).eq(0);
//   return image.updateMask(mask);
// }


// var img = landsat8Toa.first();







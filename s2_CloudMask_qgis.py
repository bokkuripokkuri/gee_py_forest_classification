import ee 
from ee_plugin import Map 

#*
 # Function to mask clouds using the Sentinel-2 QA band
 # @param {ee.Image} image Sentinel-2 image
 # @return {ee.Image} cloud masked Sentinel-2 image
 #
 def maskS2clouds(image):
    qa = image.select('QA60')

    # Bits 10 and 11 are clouds and cirrus, respectively.
    cloudBitMask = 1 << 10
    cirrusBitMask = 1 << 11

    # Both flags should be set to zero, indicating clear conditions.
    mask = qa.bitwiseAnd(cloudBitMask).eq(0) \
        .And(qa.bitwiseAnd(cirrusBitMask).eq(0))

    return image.updateMask(mask).divide(10000)


  # Map the function over one year of data and take the median.
  # Load Sentinel-2 TOA reflectance data.
  dataset = ee.ImageCollection('COPERNICUS/S2') \
                    .filterDate('2018-01-01', '2018-06-30') \
                    .filter(ee.Filter.lt('CLOUDY_PIXEL_PERCENTAGE', 20)) \
                    .map(maskS2clouds)

  rgbVis = {
    'min': 0.0,
    'max': 0.3,
    'bands': ['B4', 'B3', 'B2'],
  }

  Map.setCenter(-9.1695, 38.6917, 12)
  Map.addLayer(dataset.median(), rgbVis, 'RGB')

